// var promiseMysql = require('promise-mysql')
var mysql = require('mysql');
// mysql2 가 비동기함수 지원?
var db = mysql.createConnection({
    // host: 'localhost',
    // port: '3306',
    user: "root",
    password: "1111",
    database: "onion"
});

// async function myQueryFunction(str) {
//     return new Promise(resolve => {
//         return db.query(str, (err, result) => {
//             return resolve(result);
//         })
//     })
// }

module.exports = {
    add: {
        post: async(req, res) => {
            const user = {
                "username": req.body.username,
                "input": req.body.input
            };

            try {
                //중복 username 체크
                // const username = await req.body.username;
                // let selectname = `select ${username} from comment`;
                // let checkusername = await db.query(selectname);
                // console.log(checkusername)

                // if (checkusername) {
                //     res.status(201).send('already exist')
                //     return;
                // }

                // let add = await db.query(sql);
                let add = "insert into comment set ?"
                db.query(add, user, function(err, result) {
                    if (err) throw err;
                    else {
                        res.status(200).send('add success');
                        return;
                    }
                });
            } catch (err) {
                console.log(err);
                return;
            }
        }
    },
    get: {
        get: async(req, res) => {
            // 게시글 등록 후 list 반환하여 vue화면에 업데이트
            let selectall = "select * from comment"
            db.query(selectall, (err, result) => {
                if (err) throw err;
                else {
                    res.status(200).send(result);
                    return;
                }
            })
        }
    },
    del: {
        post: async(req, res) => {
            // 수정해야 할 사항 => 글 삭제는 유저이름으로 x, 게시글 id로 변경
            const id = req.body.co_id;
            console.log(":::::::::::::::", req.body)
                // let sql = `delete from comment where username = '${username}'`
                // let sql = `delete from comment where co_id = ${id}`
            let sql = `delete c, r from comment as c inner join reply as r on c.co_id = r.co_id where c.co_id = ${id}`

            try {
                db.query(sql, function(err, result) {
                    if (err) {
                        console.error(err);
                        throw err;
                    } else console.log(result);
                    res.status(200).send('delete success');
                })
            } catch (err) {
                console.log(err);
                return;
            }
        }
    }
}