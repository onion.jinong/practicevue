const express = require('express');

// 미들웨어
const morgan = require('morgan');
const parser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors');

// 라우터
const route = require('./routes')

const app = express();

// db연결
var db = mysql.createConnection({
    // host: 'localhost',
    // port: '3306',
    user: "root",
    password: "1111",
    database: "onion"
});

db.connect();

//schema.sql 파일 실행?

app.use(cors({
    origin: "http://localhost:8080",
    // "Access-Control-Allow-Origin": "*",
    credentials: true
}));

app.use(morgan());
app.use(parser());
app.use('/', route);


app.listen(3000, () => {
    console.log('server listen on 3000');
})

module.exports = app;