const express = require('express');
const router = express.Router();
const comment = require('./controller/comment');
const reply = require('./controller/reply');

router.post('/addcomment', comment.add.post);
router.post('/delcomment', comment.del.post);
router.get('/update', comment.get.get);


router.post('/addreply', reply.add.post);
// router.post('/delreply', reply.del.post);
// router.get('/addreply', reply.reply.get);


module.exports = router;