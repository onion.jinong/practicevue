DROP DATABASE onion;

CREATE DATABASE onion;
USE onion;

CREATE TABLE comment (
    co_id INT(11) NOT NULL AUTO_INCREMENT,
    username VARCHAR(100),
    input TEXT NULL,
    PRIMARY KEY(co_id)
);

CREATE TABLE reply (
    re_id INT(11) NOT NULL AUTO_INCREMENT,
    co_id INT(11),
    username VARCHAR(100),
    input TEXT NULL,
    PRIMARY KEY(re_id)
    -- ,FOREIGN KEY(co_id) REFERENCES comment(co_id)
);